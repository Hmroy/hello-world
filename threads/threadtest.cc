// threadtest.cc
//	Simple test case for the threads assignment.
//
//	Create two threads, and have them context switch
//	back and forth between themselves by calling Thread::Yield,
//	to illustratethe inner workings of the thread system.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "system.h"
#include "ctype.h"
#include "synch.h" // by Lisney, to include semaphore

void BusyWaitingPhilosopherHelperThread(int which);
void SemPhilosopherHelperThread(int which);
// by Lisney
void task2(int which);
void semDining(int param);
void philwait();
void one_semDining(int id, int numMeals);
// end Lisney
// Begin changes by Hunter -- P5
int accessMatrix[][];
int p5ThreadCount = 0;
int p5ObjectCount = 0;
const int NOACCESS = 0;
const int READ = 1;
const int WRITE = 2;
const int READWRITE = 3;
const int ALLOW = 1;
struct bufferObject
{
	String contents;
	Semaphore* boSem;
};
bufferObjects objArr[p5ObjectCount];
// End changes by Hunter -- P5

struct holder
{
	int id;
	int shouts;
};


void inputVerificationHelper(char * userInput, char * type){

	strcpy(type, "");
	strtok(userInput, "\n");

	bool isFloat = false;
	int ltrCnt = 0;

	for(int i = 0; i < strlen(userInput); i++){
		if(isalpha(*(userInput + i)) != 0){
			ltrCnt++;
		} else {
			if(*(userInput + i) == '-' && (isdigit(*(userInput + i+1))) != 0){
				if(i == 0){
					strcat(type, "negative ");
				} else {
					ltrCnt++;
					break;
				}
			} else if(*(userInput + i) == '.'){
				if(isdigit(*(userInput + i-1)) != 0 && isdigit(*(userInput + i+1)) != 0){
					strcat(type, "float");
					isFloat = true;
				} else {
					ltrCnt++;
					break;
				}
			} else if(*(userInput + i) == ' ' && (*(userInput + i-1) != ' ' || *(userInput + i+1) != ' ')){
				ltrCnt++;
				break;
			}
		}
	}

	if(ltrCnt == 0){
		if(!(isFloat)){
			strcat(type, "int");
		}
	} else {
		strcpy(type, "char arrays");
	}
}


void removeTrailingSpaces(char * input) {

	for(int i = strlen(input); i >= 0; i--){

		if(*(input + i) != ' ' && *(input + i) != '\0') {

			break;
		}

		if(*(input + i) == ' '){
			*(input + i) = '\0';
		}
	}


}


void replaceChars(char* input, char toReplace, char replacement){

	for(int i = 0; i < strlen(input); i++){

		if(*(input + i) == toReplace){

			*(input + i) = replacement;
		}
	}


}




void inputVerification(int which){

	char * str = new char[100];
	char * type = new char[50];

	while(1){

		strcpy(str, "");
		strcpy(type, "");
		printf("\nEnter a value: ");
		fgets(str, 100, stdin);
		strtok(str, "\n");
		removeTrailingSpaces(str);
		inputVerificationHelper(str, type);

		printf("\nYou entered: %s\nwhich is of type: %s\n\n", str, type);

		// logic for continuing
		printf("Continue (Y/N): ");
		fgets(str, 3, stdin);
		strtok(str, "\n");
		if(*str == 'y' || *str == 'Y'){
			continue;
		} else {
			break;
		}
	}

	currentThread->Finish();

}




void shoutingThreadsHelper(int param){
	holder * localHolder = (holder*)param;
	int id = localHolder->id;
	int numShouts = localHolder->shouts;

	char * words[] = {"Go and boil your bottoms, you sons of a silly person.",
			  "I fart in your general direction.",
			  "Your mother was a hamster and your father smelt of elderberries.",
			  "Help, I'm being oppressed. Come and see the violence inherent in the system.",
			  "Now go away or I shall taunt you a second time!",
			  "I don't want to talk to you no more, you empty headed animal food trough wiper.",
			  "Listen, strange women lyin' in ponds distributin' swords is no basis for a system of government.",
			  "Gentlemen, you can't fight in here! This is the War Room.",
			  "Freaking idiot.",
			  "He many look like an idiot and talk like an idiot but don't let that fool you. He really is an idiot."
			};

	int numWaitCycles = Random() % 7 + 3;
	int timesWaited = 0;


	for(int i = 0; i < numShouts; i++){
		while(timesWaited != numWaitCycles){
			timesWaited++;
			currentThread->Yield();
		}
		timesWaited = 0;
		printf("Thread %d: %s\n", id, *(words + (Random() % 10)));
	}
	currentThread->Finish();
}



void shoutingThreads(int which) {

	char* input1 = new char[100]; // number of threads
	char* input2 = new char[100]; // number of shouts
	char* type1 = new char[20];
	char* type2 = new char[20];

	printf("Enter a number of threads (1-100000): ");
	fgets(input1, 100, stdin);
	printf("Enter a number of shouts for each thread(1-100): ");
	fgets(input2, 100, stdin);
	inputVerificationHelper(input1, type1);
	inputVerificationHelper(input2, type2);



	if(strcmp(type1, "int") == 0){
		int numThreads = atoi(input1);

		if(numThreads > 100000){
				printf("that is too many threads\n");
				currentThread->Finish();

		} else if (numThreads == 0){
			printf("you must provide at least 1 thread\n");
			currentThread->Finish();
		}

		if(strcmp(type2, "int") == 0){
			int numShouts = atoi(input2);

			if(numShouts > 100){
				printf("that is too many shouts\n");
				currentThread->Finish();

			} else if (numShouts == 0){
				printf("you must provide at least 1 shout\n");
				currentThread->Finish();
			}

			for(int i = 0; i < numThreads; i++){

				holder * h = new holder();
				h->id = i;
				h->shouts = numShouts;

				Thread *t = new Thread("");
				t->Fork(shoutingThreadsHelper, int(h));
			}

		}
		else {
			printf("invalid input gonna die now\n");
			currentThread->Finish();
		}


	} else {
		printf("Invalid input. I'm gonna go die now\n");
	}

	currentThread->Finish();

}

int S;
int M;
int P;
int msg_sent = 0;
char PostOffice_M[10][5][10]; //array that tracks messages
int PostOffice_R[10][5]; //array that tracks senders
Semaphore * PostOffice_S[10]; //array of semaphores with each cell representing a person's mailbox

void Person(int id) {
	while (msg_sent != M) {
	printf("Person %i has entered the post office. \n",id);
	printf("Time to try and read a message. \n");

	PostOffice_S[id]->P(); //we need to protect

	if (PostOffice_M[id][0][0] == NULL) { //check to see if mailbox is empty
		printf("Person %i's mailbox is empty \n",id);
	}
	else {
		//need to determine how many messages we have here (Im going sicko mode)
		int msg_count = 0;
		while(PostOffice_M[id][msg_count][0]!=NULL && msg_count < M) {msg_count++;}
		for (int n = 0; n < msg_count; n++) { //first loop will go between each message
			printf("Person %i has recieved a message from Person %i that says ",id,PostOffice_R[id][n]);
			for (int z = 0; z < 7; z++) { //second loop for printing out each message
				printf("%c",(char)PostOffice_M[id][n][z]);
			}
			printf("%i",(int)PostOffice_M[id][n][7]);
			printf("\n");
		}
	}
	PostOffice_S[id]->V(); //now that we are done reading the mailbox does not need to be proctected

	//now attempt to send a message

	//deciding on who the message will go to
	int Reciever = Random() % (S);
	//deciding what message to send
	char Message = (char)(Random() % (4+1));
	//making the list of responses

printf("Person %i will attempt to send a message Pattern %i to Person %i \n",id,Message,Reciever);
printf("Person %i tries to claim  freeSpaceSemaphore[%i] \n", id, Reciever);
	//Performing P() on the reciever's semaphore to make sure their mailbox has room
	PostOffice_S[Reciever]->P();
	printf("Person %i gets and decreases freeSpaceSemaphore[%i] by 1 \n",id,Reciever);
	if (PostOffice_M[Reciever][M-1][0] != NULL) {//checking to see if the mailbox is full
		printf("This person's mailbox is full \n");
	}
	else {
	int counter = 0;
	while(PostOffice_M[Reciever][counter][0] != NULL && counter < 5) {counter++;} //finding the next available spot in the person's mailbox
	PostOffice_M[Reciever][counter][0] = 'P';
	PostOffice_M[Reciever][counter][1] = 'a';
	PostOffice_M[Reciever][counter][2] = 't';
	PostOffice_M[Reciever][counter][3] = 't';
	PostOffice_M[Reciever][counter][4] = 'e';
	PostOffice_M[Reciever][counter][5] = 'r';
	PostOffice_M[Reciever][counter][6] = 'n';
	PostOffice_M[Reciever][counter][7] = (char)Message;
	PostOffice_R[Reciever][counter] = id;
	printf("Person %i has been sent a message by Person %i \n",Reciever,id);
	msg_sent++;
	printf("Person %i has successfully updates the overall msg count to %i \n", id,msg_sent);
	PostOffice_S[Reciever]->V(); // signaling that the mailbox can be modified by something else
}
//yield the thread 3-7 times
int Wait_Cycle = Random() % (7-3+1)+3;
for (int n = 0; n < Wait_Cycle; n++) {   //the busy waiting loop
      currentThread->Yield();
    }
}
}

void Task3 (int arg) {
	char* input1 = new char[100];
	char* input2 = new char[100];
	char* input3 = new char[100];
	char* type1 = new char[20];
	char* type2 = new char[20];
	char* type3 = new char[20];

	printf("Enter the number of people participating in this simulation (1-10): ");
	fgets(input1, 100, stdin);
	printf("Enter the number of messages a mailbox can hold (1-5): ");
	fgets(input2, 100, stdin);
	printf("Enter the number of total messages that will be sent (1-5): ");
	fgets(input3, 100, stdin);

	inputVerificationHelper(input1, type1);
	inputVerificationHelper(input2, type2);
	inputVerificationHelper(input3, type3);

	if (strcmp(type1, "int") == 0) {
		P = atoi(input1);
		if (P < 1 || P > 10) {
			printf("Your input for P is either too small or too big");
			currentThread->Finish();
		}
	}
	if (strcmp(type2, "int") == 0) {
		S = atoi(input2);
		if (S < 1 || S > 5) {
			printf("Your input for S is either too small or too big");
			currentThread->Finish();
		}
	}
	if (strcmp(type3, "int") == 0) {
		M = atoi(input3);
		if (M < 1 || M > 5) {
			printf("Your input for M is either too small or too big");
			currentThread->Finish();
		}
	}
	Thread * t;
	for (int m = 0; m < P; m++){
		char a = (char) m;
		char* n = &a;
		PostOffice_S[m] = new Semaphore(n,S);
		t = new Thread{"Forked Thread"};
		t->Fork(Person,m);
	}
}


// This Thread will take input and fork to Helper, P times
void
BusyWaitingPhilosopherThread(int which)
{
	char userIn[10];
	printf("Number of Philosophers: ");
	scanf("%s", userIn);
	while(atoi(userIn) <= 1 ){
		printf("**Invalid Input**\nNumber of Philosophers: ");
		scanf("%s", userIn);
	}
	int numOfPhilosophers = atoi(userIn);

	printf("Number of Meals: ");
	scanf("%s", userIn);
	while(atoi(userIn) <= 0 ){
		printf("**Invalid Input**\nNumber of Meals: ");
		scanf("%s", userIn);
	}
	int numOfMeals = atoi(userIn);

	for(int i = 0; i < numOfPhilosophers; i++){
		Thread *t = new Thread("Shouting thread");
        t->Fork(BusyWaitingPhilosopherHelperThread, i);
	}

	bool chopsticksArr[numOfPhilosophers];
	chopsticksPtr = chopsticksArr;
	Meal = numOfMeals;
	Phil = numOfPhilosophers;
	for(int i = 0; i < numOfPhilosophers; i++){
		chopsticksArr[i] = true;
	}
	currentThread->Finish();
}

// Helper to the above
// These threads will use busy waiting loops to complete M meals (collectively)
void
BusyWaitingPhilosopherHelperThread(int which)
{
	//Setup
	printf("Philosopher %d has arrived.\n\n", which);
	currentThread->Yield();
	bool leftStick = false;
	bool rightStick = false;
	int rightAttempts = 0;
	int randLimit;
	int leftHandIndex = which;
	int rightHandIndex = which + 1;
	if(rightHandIndex == Phil){
		rightHandIndex = 0;
	}
	// Thread setup for philosopher (1.)
	printf("Philosopher %d sits.\n\n", which);
	currentThread->Yield();
	//Checks to see if all meals have been eaten (10.)
	while(Meal > 0){
		while(!leftStick || !rightStick){
			while(!leftStick){
				// if left chopstick is present, take it. Else busy wait
				if(*(chopsticksPtr + leftHandIndex)){
					// Take chopstick (Left) (2.)
					*(chopsticksPtr + leftHandIndex) = false;
					leftStick = true;
					printf("Philosopher %d has grabbed chopstick # %d.\n\n", which, leftHandIndex);
				} else{
					currentThread->Yield();
				}
			}
			while(!rightStick && rightAttempts <= 5){

				// if left chopstick is present, take it. Else busy wait
				if(*(chopsticksPtr + rightHandIndex)){
					rightAttempts = 0;
					// Take chopstick (Right) (3.)
					*(chopsticksPtr + rightHandIndex) = false;
					rightStick = true;
					printf("Philosopher %d has grabbed chopstick # %d.\n\n", which, rightHandIndex);
				} else{
					rightAttempts++;
					if( rightAttempts <= 5 )currentThread->Yield();
				}
			}
			if(rightAttempts > 5 && !rightStick){
				rightAttempts = 0;
				leftStick = false;
				*(chopsticksPtr + leftHandIndex) = true;
				printf("Philosopher %d has replaced chopstick # %d, because he cannot find chopstick %d\n\n", which, leftHandIndex, rightHandIndex);
				currentThread->Yield();
			}
		}
		if(Meal > 0){
			// Eat (4. & 5.)
			printf("Philosopher %d is Eating.", which);
			mealsEaten++;
			Meal--;
			printf(" (%d meals eaten so far.)\n\n", mealsEaten);
			randLimit = Random() % 5;
			randLimit += 3;
			for(int i = randLimit; i != 0; i--){
				currentThread->Yield();
			}
		}else{
			printf("Philosopher %d cannot eat, he has noticed all meals have been eaten.\n\n", which);
		}
		// Replace chopsticks
		// (6.)
		*(chopsticksPtr + leftHandIndex) = true;
		leftStick = false;
		printf("Philosopher %d has replaced chopstick # %d", which, leftHandIndex);
		//(7.)
		*(chopsticksPtr + rightHandIndex) = true;
		rightStick = false;
		printf(" and %d.\n\n", rightHandIndex);


		// Thinking occurs after eating attempt fails OR succeeds(8. & 9.)
		printf("Philosopher %d is Thinking.\n\n", which);
		randLimit = Random() % 5;
		randLimit += 3;
		for(int i = randLimit; i != 0; i--){
			currentThread->Yield();
		}


	}
	// Philosopher must wait until all others are done.
	printf("Philosopher %d is waiting to leave.\n\n", which);
	Phil--;
	while(Phil > 0){
		currentThread->Yield();
	}
	// leave table (11.)
	printf("Philosopher %d is now leaving.\n\n", which);
	currentThread->Finish();
}

// being code changes by Lisney Borroto
struct holder2
{
	int id;
	int meals;
	int philosophers;
};

void task2(int which) {
	char userIn[10];
	printf("Number of Philosophers (1-10): ");
	scanf("%s", userIn);
	while((atoi(userIn) <= 0) || (atoi(userIn) > 10)){
		printf("**Invalid Input**\nNumber of Philosophers: ");
		scanf("%s", userIn);
	}
	int numOfPhilosophers = atoi(userIn);

	printf("Number of Meals (1-10): ");
	scanf("%s", userIn);
	while((atoi(userIn) <= 0) || (atoi(userIn) > 10)){
		printf("**Invalid Input**\nNumber of Meals: ");
		scanf("%s", userIn);
	}

	int numOfMeals = atoi(userIn);

	for(int i = 0; i < numOfPhilosophers; i++){
		holder2 *h = new holder2();
		h->id = i;
		h->meals = numOfMeals;
		h->philosophers = numOfPhilosophers;
		Thread *t = new Thread("thre-llo");
    t->Fork(semDining, int(h));
	}

	philsready = new Semaphore("thread", 0); // semaphore for synch leaving

	//Semaphore **chopsticks;  assigned global var
	chopsticks = new Semaphore*[numOfPhilosophers];
	for (int i = 0; i < numOfPhilosophers; i++) {
		chopsticks[i] = new Semaphore("hi", 1);
	}

	currentThread->Finish();

}

void philwait() {
	int numWait = Random() % 5 + 3; // random number between 3 and 7

	for (int i = numWait; i > 0; i--) {  //  eat or think
		currentThread->Yield();
	}

}

void one_semDining(int id, int numMeals) {
	printf("Philosopher %d has arrived.\n", id);
	while (numMeals > 0) {
		chopsticks[0]; // pick up left chopstick
		printf("Philosopher %d has picked up left chopstick %d\n", id, id);
		chopsticks[1]; // pick up right chopstick
		printf("Philosopher %d has picked up right chopstick %d\n", id, id+1);

		printf("Philosopher %d has begun eating. They have %d meal(s) left.\n", id, numMeals-1);
		philwait();
		numMeals--;

		chopsticks[0]; // put down left chopstick
		printf("Philosopher %d has put down left chopstick %d\n", id, id);
		chopsticks[1]; // put down right chopstick
		printf("Philosopher %d has put down right chopstick %d\n", id, id+1);

		printf("Philosopher %d has begun thinking\n",id);
		philwait();
	}
	printf("Philosopher %d has left the table.\n",id);
	currentThread->Finish();

}

void semDining(int param) {
	holder2* localHolder = (holder2*)param;
	int id = localHolder->id;
	int numMeals = localHolder->meals;
	int numPhil = localHolder->philosophers;

	if (numPhil == 1) {
		one_semDining(id, numMeals);
	}

	printf("Philosopher %d has arrived.\n", id);

	currentThread->Yield(); // all philosophers sit at the table

	while (numMeals > 0) {
		if ((id==numPhil-1) && (numPhil!=1)){ // to break symmetry that causes deadlock, last philospher will pick up the right chopstick first
			chopsticks[(id+1) % numPhil]->P(); // pick up right chopstick when available
			printf("Philosopher %d has picked up right chopstick %d\n", id, (id+1) % numPhil);
			chopsticks[id]->P(); // pick up left chopstick when available
			printf("Philosopher %d has picked up left chopstick %d\n", id, id);
		} else {
			chopsticks[id]->P(); // pick up left chopstick when available
			printf("Philosopher %d has picked up left chopstick %d\n", id, id);
			chopsticks[(id+1) % numPhil]->P(); // pick up right chopstick when available
			printf("Philosopher %d has picked up right chopstick %d\n", id, (id+1) % numPhil);
		}

		printf("Philosopher %d has begun eating. They have %d meal(s) left.\n", id, numMeals-1);
		philwait();
		numMeals--;

		chopsticks[id]->V(); // put down left chopstick
		printf("Philosopher %d has put down left chopstick %d\n", id, id);
		chopsticks[(id+1) % numPhil]->V(); // put down right chopstick
		printf("Philosopher %d has put down right chopstick %d\n", id, (id+1) % numPhil);

		printf("Philosopher %d has begun thinking\n",id);
		philwait();
	} // end while loop

	philcount++;
	if (philcount < numPhil) {
		philsready->P();
	}
	philsready->V();

	printf("Philosopher %d has left the table.\n",id);
	currentThread->Finish();
}
// END CODE CHANGES by Lisney Borroto



//----------------------------------------------------------------------
// SimpleThread
// 	Loop 5 times, yielding the CPU to another ready thread
//	each iteration.
//
//	"which" is simply a number identifying the thread, for debugging
//	purposes.
//----------------------------------------------------------------------

void
SimpleThread(int which)
{
    int num;

    for (num = 0; num < 5; num++) {
	printf("*** thread %d looped %d times\n", which, num);
        currentThread->Yield();
    }
}

//----------------------------------------------------------------------
// AccessMatrixThread
// 	Threads will attempt to access Objects and switch domains.
//	Before doing so they will check for permission.
//  Permissions will be located from access matrix
//
//	"which" is simply a number identifying the thread, for debugging
//	purposes.
//----------------------------------------------------------------------

void
AccessMatrixThread(int which)
{	
	int id = which;


	// Randomize access vs domain switching
    int accessOrSwitch = Random() % 2;
	if(accessOrSwitch== 0){
		// Access
		int readOrWrite = Random() % 2;
		int onWho = Random() % p5ObjectCount;
		if(readOrWrite == 0){
			//read from onWho
			printf("D%i would like to read from obj%i",id,onWho);
			if(accessMatrix[id][onWho]== READ || (accessMatrix[id][onWho]== READWRITE){
				//permission granted
				objArr[onWho]->doSem->P();
				printf("D%i reads: ",id);
				print(objArr[onWho]->contents);
				printf("from Object%i\n",onWho);
				philwait(); // reused old busy waiting loop
				objArr[onWho]->doSem->V();
			}else{
				printf("D%i does not have proper access\n", id);
			}
		}else{
			printf("D%i would like to write to obj%i",id,onWho);
			if(accessMatrix[id][onWho]== WRITE || (accessMatrix[id][onWho]== READWRITE){
				//permission granted
				String str = "D" + id + " was here ";
				objArr[onWho]->doSem->P();
				printf("D%i writes: ",id);
				print(str);
				objArr[onWho]->contents+=str;
				printf("on Object%i\n",onWho);
				philwait(); // reused old busy waiting loop
				objArr[onWho]->doSem->V();
			}else{
				printf("D%i does not have proper access\n", id);
			}
		}

	}else{
		//Domain switch
		int toWho = Random() % p5ThreadCount;
		// switch to toWho
		printf("D%i would like to switch to D%i",id,onWho);
		if(accessMatrix[id][p5ObjectCount + toWho]== ACCESS){
				//permission granted
				printf("D%i swtiches to D%i: ",id,toWho);
				id = toWho;
				philwait(); // reused old busy waiting loop
			}else{
				printf("D%i does not have proper access\n", id);
			}
		
	}

}



//----------------------------------------------------------------------
// ThreadTest
// 	Invoke a test routine.
//----------------------------------------------------------------------

// Begin changes by Hunter -- P5
void
ThreadTest()
{
    DEBUG('t', "Entering ThreadTest modified for Permission Handling");
	
	// rand threadcount [3,7]
	int p5ThreadCount = Random() % 5 + 3;
	// rand objCount [3,7]
	int p5ObjectCount =  Random() % 5 + 3;
	//create Objects
	bufferObjects* objArr[p5ObjectCount];
	for(int i = 0; i < p5ObjectCount ;i++){
			objArr[i] = struct bufferObject{  "", new Semaphore(1)};
	}

	if (whichTask == 1) {
		// Option 1: Access Matrix

		// init matrix
		accessMatrix = int[p5ThreadCount][p5ObjectCount + p5ThreadCount];

		// N x M = NoAccess or Read or Write or Read&Write
		for(int i = 0; i < p5ThreadCount; i++ ){
			for(int j = 0; j < p5ObjectCount; j++ ){

				accessMatrix[i][j] = Random() % 4;
			}
		}

		// Domain switching entries = NoAccess or access
		for(int i = 0; i < p5ThreadCount; i++ ){
			for(int j = 0; j < p5ThreadCount; j++ ){
				if(i==j){
					accessMatrix[i][p5ObjectCount + j] = 1;
				}else{
					if(i==j){
					accessMatrix[i][p5ObjectCount + j] = Random() % 2;
					}
				}
				
			}
		}

		// fork threads
		for(int i = 0; i < p5ThreadCount; i++ ){
			Thread* threadptr = newThread("DomianThread");
			threadptr->Fork(AccessMatrixThread, i)
		}
		// Calling thread...Finish
		currentThread->Finish();

	} else if (whichTask == 2) {
		// Option 2: Access List
		
		currentThread->Finish();

	} elseif (whichTask == 3) {
		// Option 3: Capability Lists
		
		currentThread->Finish();

	} 
// End changes by Hunter -- P5

	/* if (whichTask == 1) {
		// Option 1: Input Evaluation
		Thread *t = new Thread("inputValidation thread");
		t->Fork(inputVerification, 0);
		currentThread->Finish();

	} else if (whichTask == 2){
		// Option 2: Random Shouting
		Thread *t = new Thread("shoutingThreads thread");
		t->Fork(shoutingThreads, 0);
		currentThread->Finish();
	} else if (whichTask == 3){
		// Option 3: Philosopher w/ Busy waiting
		Thread *t = new Thread("busyPhilosophers thread");
		t->Fork(BusyWaitingPhilosopherThread, 0);
		currentThread->Finish();
		// code by Lisney
	} else if (whichTask == 4){
		// Option 4: Philosopher w/ Semaphores
		Thread *t = new Thread("semaphorePhilosophers thread");
		t->Fork(task2, 0);
		currentThread->Finish();

	 } else if (whichTask == 5){
	 	// Option 5: Post Office
	 	Thread *t = new Thread("postOffice thread");
	 	t->Fork(Task3, 0);
	 	currentThread->Finish();

	 } */
	 //else if (whichTask == 6){
	 	// Option 6: Read Write
	 //	Thread *t = new Thread("readWrite thread");
	 	//t->Fork(ReadWriteThread, 0);
	 	//currentThread->Finish();

//	}
	else {
		printf("invalid input\n");
		currentThread->Finish();
	}

}
